function importTemplate(dir, filename, dlmstr)
% IMPORT  - import data from text file
    
% DIR - string of directory terminated with forward slash, for
% example '../src/'
    
% FILENAME - string with filename and extension, e.g. 'data.csv'
    
% DLMSTR - Delimiter used e.g. ',' or '\t'

    function fid = openFile(dir, filename)
    % Open file, error out if cannot be acheived. If succesful return
    % file identification number.
        
        filestr = strcat(dir,filename);
        fid = fopen(filestr,'rt');
    
        if fid > 0                          % file opened sucessfully
            fprintf('Importing %s ...\n',filestr);
        else
            estr = sprintf('Cannot read file %s', filestr);
            error(estr);
        end
    end
    
    function val_fgetlLength(target, cur_line, n, str);
    % Validate length of line

    % target   - expected length
    % cur_line - current line (cell) delimited fgetl() output
    % n        - current line number
    % str      - string for error message

        if length(cur_line) ~= target

            estr_incorrDataFields = ...
                sprintf('Line %d: expected %d %s, only %d present', ...
                        n, target, str, length(cur_line));
            
            error(estr_incorrDataFields);
        end
    end

    function skipLine = val_cellEmpty(field, action, n)
    % Check if the cell is empty and either warn or error depending on
    % the action string provided. Feedback line n to user in
    % message.

    % Accepts following actions as strings:
    %    error - error out
    %    skip  - ignore line with missing value break out of while loop
    %    warn  - warn user but import cell
        
        estr_empty = sprintf('in line %d, empty data field', n);

        skipLine = false;
            
        if isempty(field)

            switch action
              case 'error'
                error(estr_empty);
                
              case 'warn'
                warning(estr_empty);
                
              case 'skip'
                estr_skip = sprintf('%s. Row %d data omitted', ... 
                                    estr_empty, n);
                warning(estr_skip);

                skipLine = true;

              otherwise
                error('Incorrect string to val_cellEmpty()');
            end
        end
    end
    
    %% Open file and Import Header Lines

    fid = openFile(dir, filename);

    headers = strsplit(fgetl(fid),dlmstr);
    
    h_count= 2;                         % used to track current line from fgetl()

    %% Pre allocate memory for data

    N = 1000;                           % not required if eof defined
    % N = headers{3}

    date     = zeros(N, 1);
    param1   = zeros(N, 1);
    param2   = zeros(N, 1);

    %% Import data fields

    n = 1;

    while ~feof(fid)                    % check eof slower than n < N
    % while n < n + 1
        
        noFieldsPerLine = 3;

        cur_line = fgetl(fid);
        cur_line = strsplit(cur_line, ',', ...
                            'CollapseDelimiters', false);
        
        val_fgetlLength(noFieldsPerLine, cur_line, n, 'data columns');
        
        % Check not empty and import data
        skipLine =  [val_cellEmpty(cur_line{1}, 'skip', n), ...
                     val_cellEmpty(cur_line{2}, 'warn', n), ...
                     val_cellEmpty(cur_line{3}, 'warn', n)]
        
        if  any(skipLine); continue; end;
        
        % Convert formatting
        date(n)   = datenum(curline{1},'DD-MM-YY');
        param1(n) = str2double(curline{2});
        param2(n) = str2double(curline{3});

        n = n + 1;

        

    end

    %% Trim excess array elements if eof not defined
    
    N = n - 1;
    
    date = date(N);
    param1 = param1(N);
    param2 = param2(N);
            
end